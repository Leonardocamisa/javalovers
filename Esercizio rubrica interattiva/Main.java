import java.util.Scanner;
import java.util.ArrayList;


//  punto di avvio dell'applicazione
public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        String name;
        String surname;
        int age;

        ArrayList<Persona> rubrica = new ArrayList<Persona>();
        
        System.out.println(" - - - - RUBRICA - - - - -");
        printMenu();
        String option;
        
        do {
            System.out.println(" Selezione un'opzione");
            System.out.print(" ~~> ");
            option = input.nextLine();
            switch(option) {
                case "l":
                rubrica.forEach((contatto) -> {
                    contatto.show();
                    });
                    break;
                case "c":
                    System.out.print("Digita il tuo nome: ");
                    name = input.nextLine();
                    System.out.print("Digita il tuo cognome: ");
                    surname = input.nextLine();
                    System.out.print("Digita la tua età: ");
                    age = input.nextInt();
                    input.nextLine();
                    Persona contatto = new Persona("name" ,"surname", age);
                    rubrica.add(contatto);
                    break;
                case "q":
                    System.out.println(" ARRIVEDERCI ");
                    break;
                case "h":
                printMenu();
                default:
                    printMenu();
            }
            
        } while(!option.equals("q"));

    }


    public static void printMenu() {
        String menu = """ 
            l) visualizza contatti
            c) crea nuovo contatto
            h) pagina di aiuto
            q) esci
        """;
        System.out.println(menu);
    }
}