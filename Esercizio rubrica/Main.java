import java.util.Scanner;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int lunghezzaRubrica;
        String name;
        String surname;
        int age;
        
        System.out.print("inserisci la lunghezza della libreria: ");

        lunghezzaRubrica = input.nextInt();
        input.nextLine();

        ArrayList<Persona> rubrica = new ArrayList<Persona>();

        for (int i = 0; i < lunghezzaRubrica; i++ ) {
              System.out.print("Digita il tuo nome: ");
              name = input.nextLine();
              System.out.print("Digita il tuo cognome: ");
              surname = input.nextLine();
              System.out.print("Digita la tua età: ");
              age = input.nextInt();
              input.nextLine();
              Persona contatto = new Persona("name" ,"surname", age);
              rubrica.add(contatto);
            }

        rubrica.forEach((contatto) -> {
            contatto.show();
        });
        
    }

}