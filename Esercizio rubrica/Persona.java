public class Persona {

    public String name;
    public String surname;
    public int age;

    public Persona(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void show(){
        System.out.println(name + " " + surname + " " + age);
    }
}