import java.util.Arrays;
import java.util.function.Consumer;

public class ArrayString {

    private int nElements = 0;
    private String[] array;

    public ArrayString (int length) {
        array = new String[length];
    }

    public ArrayString add(String element) {
        if (nElements < this.array.length) {
            this.array[nElements] = element;
            nElements++;
        } else {
            this.expand(5);
            this.add(element);
        }
        return this;
    }

    public ArrayString add(String... elements) {
        for(int i = 0; i < elements.length; i++) {
            this.add(elements[i]);
        }
        return this;
    }

    public ArrayString add(char element) {
        String x = String.valueOf(element);
        this.add(x);
        return this;
    }

    public ArrayString removeLast() {
        if(nElements > 0) {
            nElements = nElements - 1;
        }
        return this;
    }

    public ArrayString print() {
        this.forEach((element) -> System.out.print(element + " "));
        System.out.println();
        return this;
    }

    public ArrayString forEach(Consumer<String> action) {
        for(int i = 0; i < this.nElements; i++) {
            action.accept(this.array[i]);
        }
        return this;
    }

    public ArrayString expand(int amount) {
        int newSize = this.array.length + amount;
        String[] newArr = new String[newSize];
        for (int i = 0; i < nElements; i++) {
            newArr[i] = this.array[i];
        }
        this.array = newArr;
        return this;
    }

    public int getLength() {
        return this.array.length;
    }

    public int count() {
        return this.nElements;
    }

    public ArrayString clone() {
        ArrayString newArr = new ArrayString(this.getLength());
        for (int i = 0; i < nElements; i++) {
            newArr.add(this.array[i]);
        }
        return newArr;
    }

    public ArrayString withoutDuplicates() {
        ArrayString uniques = new ArrayString(this.nElements);
        this.forEach((element) -> {
            if (!uniques.search(element).found) {
                uniques.add(element);
            }
        });
        return uniques;
    }

    public ArrayString removeDuplicates() {
        for (int i = 0; i < this.nElements; i++) {
            for (int j = i+1; j < this.nElements; j++) {
                if(this.array[j] == this.array[i]) {
                    this.removeAt(j);
                    j--;
                }
            }
        }
        return this;
    }

    public int countDuplicates() {
        ArrayString clone = this.withoutDuplicates();
        return this.count() - clone.count();
    }

    public SearchElement search(String x) {
        SearchElement element = new SearchElement();
        for (int i = 0; i < nElements && (element.found != true); i++) {
            if (this.array[i] == x) {
                element.found = true;
                element.index = i;
            }
        }
        return element;
    }

    public ArrayString removeIfExists(String x) {
        SearchElement element = this.search(x);
        if (element.found) {
            this.removeAt(element.index);
        }
        return this;
    }

    //  Rimuove l'elemento all'INDICE index
    public ArrayString removeAt(int index) {
        if (index >= 0 && index < nElements) {
            // scambia gli elementi ai due indici dati
            this.swap(index, nElements-1);  
            this.removeLast();
        }
        return this;  
    }

    //  Rimuove tutte le occorrenze del valore value
    public ArrayString remove(String value) {
        SearchElement element = this.search(value);
        while(element.found) {
            this.removeAt(element.index);
            element = this.search(value);
        }
        return this;
    }

    public ArrayString sort() {
        Arrays.sort(this.array);
        return this;
    }

    public ArrayString swap(int i, int j) {
        if(i >= 0 && i < nElements && j >= 0 && j < nElements) {
            String t = this.array[i];
            this.array[i] = this.array[j];
            this.array[j] = t;
        }
        return this;
    }

    
}