import java.util.Arrays;
import java.util.function.Consumer;

public class ArrayNumero {

    private int nElements = 0;
    private int[] array;

    public ArrayNumero (int length) {
        array = new int[length];
    }

    public ArrayNumero add(int element) {
        if (nElements < this.array.length) {
            this.array[nElements] = element;
            nElements = nElements + 1;
        }
        else {
            this.expand(5);
            this.add(element);  //  Ricorsione a add per inserire l'elemento
            //  potrei riscrivere le righe di codice sopra per inserire direttamente l'elemento, ma la chiamata ricorsiva è più bella.
        }
        return this;
    }

    //  overloading 
    public ArrayNumero add(int... elements) {
        for(int i = 0; i < elements.length; i++) {
            this.add(elements[i]);
        }
        return this;
    }

    public ArrayNumero add(char element) {
        int x = Integer.parseInt(String.valueOf(element));
        this.add(x);
        return this;
    }

    //  rimuove l'ultimo elemento dall'array
    public ArrayNumero removeLast() {
        if(nElements > 0) {
            // this.array[nElements] = 0;
            nElements = nElements - 1;
        }
        return this;
    }

    public ArrayNumero print() {
        this.forEach((element) -> System.out.print(element + " "));
        System.out.println();
        return this;
    }

    public SearchElement search(int x) {
        SearchElement element = new SearchElement();
        for (int i = 0; i < nElements && (element.found != true); i++) {
            if (this.array[i] == x) {
                element.found = true;
                element.index = i;
            }
        }
        return element;
    }

    public ArrayNumero removeIfExists(int x) {
        SearchElement element = this.search(x);
        if (element.found) {
            this.removeAt(element.index);
        }
        return this;
    }

    //  Rimuove l'elemento all'INDICE index
    public ArrayNumero removeAt(int index) {
        if (index >= 0 && index < nElements) {
            // scambia gli elementi ai due indici dati
            this.swap(index, nElements-1);  
            this.removeLast();
        }
        return this;  
    }

    //  Rimuove tutte le occorrenze del valore value
    public ArrayNumero remove(int value) {
        SearchElement element = this.search(value);
        while(element.found) {
            this.removeAt(element.index);
            element = this.search(value);
        }
        return this;
    }

    public ArrayNumero swap(int i, int j) {
        if(i >= 0 && i < nElements && j >= 0 && j < nElements) {
            int t = this.array[i];
            this.array[i] = this.array[j];
            this.array[j] = t;
        }
        return this;
    }

    public ArrayNumero sort() {
        Arrays.sort(this.array);
        return this;
    }

    public int average() {
        return this.somma() / nElements;
    }

    public int somma() {
        int somma = 0;
        for (int i = 0; i < nElements; i++) {
            somma += this.array[i];
        }
        return somma;
    }


    //  1 2 3 4 5 6
    // 1 2 3 
    // TODO:
    public ArrayNumero subArray(int i, int j) {
        ArrayNumero sub = null;
        if (i >= 0 && i < nElements && j >= 0 && j < nElements) {
            if (i < j) {
                sub = new ArrayNumero((j - i) + 1);
                for (int w = i; w <= j; w++) {
                    sub.add(this.array[w]);
                }
            }
        }
        return sub;
    }

    public ArrayNumero expand(int amount) {
        int newSize = this.array.length + amount;
        int[] newArr = new int[newSize];
        for (int i = 0; i < nElements; i++) {
            newArr[i] = this.array[i];
        }
        this.array = newArr;
        return this;
    }

    public int getLength() {
        return this.array.length;
    }

    public int count() {
        return this.nElements;
    }

    public ArrayNumero clone() {
        ArrayNumero newArr = new ArrayNumero(this.getLength());
        for (int i = 0; i < nElements; i++) {
            newArr.add(this.array[i]);
        }
        return newArr;
    }

    public ArrayNumero withoutDuplicates() {
        ArrayNumero uniques = new ArrayNumero(this.nElements);
        this.forEach((element) -> {
            if (!uniques.search(this.array[i]).found) {
                uniques.add(this.array[i]);
            }
        })
        return uniques;
    }

    public ArrayNumero removeDuplicates() {
        for (int i = 0; i < this.nElements; i++) {
            for (int j = i+1; j < this.nElements; j++) {
                if(this.array[j] == this.array[i]) {
                    this.removeAt(j);
                    j--;
                }
            }
        }
        return this;
    }

    public int countDuplicates() {
        ArrayNumero clone = this.withoutDuplicates();
        return this.count() - clone.count();
    }

    // public ArrayNumero forEachDuplicate(Consumer<ArrayNumero, Integer> callBack) {
    //     for (int i = 0; i < this.nElements; i++) {
    //         for (int j = i+1; j < this.nElements; j++) {
    //             if(this.array[j] == this.array[i]) {
    //                 callBack.accept(this, j);
    //             }
    //         }
    //     }
    //     return this;
    // }

    // TODO next lesson
    public ArrayNumero forEach(Consumer<Integer> action) {
        for(int i = 0; i < this.nElements; i++) {
            action.accept(this.array[i]);
        }
        return this;
    }
    




}
