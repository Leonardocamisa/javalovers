import java.util.Scanner;
public class InterazioneUtente {
    public static Scanner sc = new Scanner(System.in);

    public static Veicolo getVeicoloUtente() {
        
        int option, indiceVeicolo;
        String utenteName;

        Veicolo utente = null;
        printMenu();

        do {
            System.out.print("  ~~> ");
            option = sc.nextInt();
            switch(option) {
                case 1:
                    do {
                        printModelliVeicolo(Database.automobili);
                        indiceVeicolo = sc.nextInt();
                        if (indiceVeicolo < 0 || indiceVeicolo >= Database.automobili.length) {
                            printError(" Seleziona un indice valido per il veicolo");
                        }
                    } while(indiceVeicolo < 0 || indiceVeicolo >= Database.automobili.length);
                    utente = Database.automobili[indiceVeicolo];
                    utenteName = getUtenteName();
                    utente.setName(utenteName);
                    break;

                case 2:
                    do {
                        printModelliVeicolo(Database.motociclette);
                        indiceVeicolo = sc.nextInt();
                        if (indiceVeicolo < 0 || indiceVeicolo >= Database.motociclette.length) {
                            printError(" Seleziona un indice valido per il veicolo");
                        }
                    } while(indiceVeicolo < 0 || indiceVeicolo >= Database.motociclette.length);
                    utente = Database.motociclette[indiceVeicolo];
                    utenteName = getUtenteName();
                    utente.setName(utenteName);
                    break;
                
                case 3:
                    do {
                        printModelliVeicolo(Database.velivoli);
                        indiceVeicolo = sc.nextInt();
                        if (indiceVeicolo < 0 || indiceVeicolo >= Database.velivoli.length) {
                            printError(" Seleziona un indice valido per il veicolo");
                        }
                    } while(indiceVeicolo < 0 || indiceVeicolo >= Database.velivoli.length);
                    utente = Database.velivoli[indiceVeicolo];
                    utenteName = getUtenteName();
                    utente.setName(utenteName);
                    break;

                default:
                    printError(" Hai selezionato un'opzione non valida (seleziona da 1 a 3)");

            }
        } while(option < 1 || option > 3);

        
        return utente;
    }

    public static void printModelliVeicolo(Veicolo[] modelli) {
        for(int i = 0; i < modelli.length; i++) {
            System.out.println( i + ") " + modelli[i].getModello());
        }
        System.out.println();
    }

    public static String getUtenteName() {
        sc.nextLine();
        System.out.print(" SCRIVI IL TUO NOME: ");
        return sc.nextLine();
    }

    public static void printMenu() {
        String menu = """
             - - - - - - - Real Racing X3 - - - - - - - - -

             Seleziona un tipo di veicolo per partecipare:

             1) Automobile
             2) Motocicletta
             3) Velivolo

             - - - - - - - - - - - - - - - - - - - - - - - 
        """;
        System.out.println(menu);
    }

    public static void printError(String error) {
        System.out.println(Colors.ANSI_RED + error + Colors.ANSI_RESET);
    }
}