public class Database {

    public static Automobile[] automobili = {
        new Automobile("[BOT] Marco", "Audi TT"),
        new Automobile("[BOT] Sofia", "Abarth 595 C"),
        new Automobile("[BOT] Aurora", "Aston Martin Vantage Roadster"),
        new Automobile("[BOT] Tommaso", "Audi R8 Spyder"),
        new Automobile("[BOT] Edoardo", "Audi e-tron GT"),
        new Automobile("[BOT] Matilde", "Bentley Continental GT")
    };

    public static Moto[] motociclette = {
        new Moto("[BOT] Giorgia", "Ducati Panigale V4"),
        new Moto("[BOT] Andrea", "Yamaha R7"),
        new Moto("[BOT] Ginevra", "Kawasaki Ninja ZX-10R"),
        new Moto("[BOT] Simone", "Suzuki G6X 1300"),
        new Moto("[BOT] Leonardo", "Ducati Supersport S")
    };

    public static Velivolo[] velivoli = {
        new Velivolo("[BOT] Liam", "E-LSA Antares USA Ranger"),
        new Velivolo("[BOT] William", "S-LSA Evektor SportStar"),
        new Velivolo("[BOT] Addo", "L-LSA Zlin Aviation Savage Cub"),
        new Velivolo("[BOT] Ajene", "Aerotrek A240"),
        new Velivolo("[BOT] Fallou", "Zodiac 650B")
    };
}