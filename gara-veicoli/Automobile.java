
public class Automobile extends Veicolo {

    private static final int velocitaMin = 80;
    private static final int velocitaMax = 140;

    public Automobile(String nome, String modello) {
        super(nome, modello, "Automobile");
    }

    @Override
    public int getVelocitaMin() {
        return velocitaMin;
    }

    @Override
    public int getVelocitaMax() {
        return velocitaMax;
    }

}