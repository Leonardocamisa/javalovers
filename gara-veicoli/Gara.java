import java.util.Random;
import java.util.Arrays;
public class Gara {

    private int partecipantiOnline = 0;
    private int lunghezzaPercorso = 50;
    private Veicolo[] partecipanti;

    public Gara(int numeroPartecipanti){
        this.partecipanti = new Veicolo[numeroPartecipanti];
    }

    public Gara(int numeroPartecipanti, int lunghezzaPercorso) {
        this(numeroPartecipanti);
        this.lunghezzaPercorso = lunghezzaPercorso;
    }

    public Gara add(Veicolo partecipante) {
        if(this.partecipantiOnline < this.partecipanti.length) {
            this.partecipanti[partecipantiOnline] = partecipante;
            this.partecipantiOnline = this.partecipantiOnline + 1;
        }
        return this;
    }

    public void generaPartecipantiBot() {
        for(int i = 0; i < this.partecipanti.length - 1; i++) {
            Veicolo newBot;
            do {
                newBot = this.generaPartecipanteBot();
            } while(this.search(newBot));
            this.add(newBot);
        }
    }

    // TODO: se un bot esiste già, generane un altro
    private Veicolo generaPartecipanteBot() {
        Random r = new Random();
        Veicolo bot;
        // random tra 1 e 3 (inclusi)
        int tipoPartecipante = r.nextInt(4 - 1) + 1;
        int veicoloIndex;
        if(tipoPartecipante == 1) {
            // Automobile
            veicoloIndex = r.nextInt((Database.automobili.length - 1));
            bot = Database.automobili[veicoloIndex];
        } else if (tipoPartecipante == 2){
            // Moto
            veicoloIndex = r.nextInt((Database.motociclette.length - 1));
            bot = Database.motociclette[veicoloIndex];
        }
        else {
            // Velivolo
            veicoloIndex = r.nextInt((Database.velivoli.length - 1));
            bot = Database.velivoli[veicoloIndex];
        }
        return bot;
    }

    public boolean search(Veicolo partecipante) {
        boolean found = false;
        for (int i = 0; i < partecipantiOnline && !found; i++) {
            if(partecipanti[i].equals(partecipante)) {
                found = true;
            }
        }
        return found;
    }

    public void startRace() {
        System.out.println(Colors.ANSI_YELLOW + " Gara iniziata " + Colors.ANSI_RESET);
        for (int i = 0; i < partecipantiOnline; i++) {
            this.partecipanti[i].generaVelocita();
        }
    }

    public void finishRace() {
        this.ordinaClassica();
        System.out.println(" LA GARA È TERMINATA: \n\n Classifica: ");
        for(int i = 0; i < partecipantiOnline; i++) {
            System.out.println( Colors.ANSI_YELLOW + "- - - - - - - - - - -" + Colors.ANSI_RESET);
            System.out.println( i+1 + " Classificato ");
            System.out.print(this.partecipanti[i]);
            System.out.println("Tempo di arrivo: " + calcolaTempoDiArrivo(this.partecipanti[i]));
        }
        System.out.println( Colors.ANSI_YELLOW + "- - - - - - - - - - -" + Colors.ANSI_RESET);
    }

    public double calcolaTempoDiArrivo(Veicolo veicolo) {
        double tempo = this.lunghezzaPercorso / veicolo.getSpeed();
        tempo = tempo / 3600;
        return Math.floor(tempo * 100) / 100;
    }

    public void print() {
        for(int i = 0; i < partecipantiOnline; i++) {
            System.out.println( Colors.ANSI_YELLOW + "- - - - - - - - - - -" + Colors.ANSI_RESET);
            System.out.print(this.partecipanti[i]);
        }
    }

    public void ordinaClassica() {
        Arrays.sort(this.partecipanti);
    }

}