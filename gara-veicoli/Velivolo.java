
public class Velivolo extends Veicolo {

    private static final int velocitaMin = 100;
    private static final int velocitaMax = 180;

    public Velivolo(String nome, String modello) {
        super(nome, modello, "Velivolo");
    }

    @Override
    public int getVelocitaMin() {
        return velocitaMin;
    }

    @Override
    public int getVelocitaMax() {
        return velocitaMax;
    }
}