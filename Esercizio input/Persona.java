public class Persona {

    public String name;
    public String surname;

    public Persona(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }
}