import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

         Scanner input = new Scanner(System.in);

         Persona inserita = new Persona("name", "surname");

         System.out.print("Digita il tuo nome: ");

         String name;

         inserita.name = input.nextLine();

         System.out.print("Digita il tuo cognome: ");

         String surname;

         inserita.surname = input.nextLine();

         System.out.print("Aggiunto un utente con i dati: " + inserita.name + " " + inserita.surname);
    }

}