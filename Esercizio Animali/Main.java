public class Main {

    public static void main(String[] args) {

        Animale cane = new Animale("cane", 7, true);

        cane.printInfo();

        Animale tigre = new Animale("tigre", 4, false);

        tigre.printInfo();

        Animale gatto = new Animale("gatto", 11, true);

        gatto.printInfo();

    }
}