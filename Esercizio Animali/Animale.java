public class Animale {

    public String specie;
    public int age;
    public boolean isFriendly;

    public Animale(String specie, int age, boolean isFriendly) {
        this.specie = specie;
        this.age = age;
        this.isFriendly = isFriendly;
    }

    public void printInfo() {
        System.out.println(specie + ", " + age + " anni," + " friendly: " + isFriendly);
    }
}