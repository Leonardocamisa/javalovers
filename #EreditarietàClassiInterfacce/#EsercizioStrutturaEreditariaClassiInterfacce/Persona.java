public abstract class Persona implements Human {

    protected int age;
    protected String name;
    protected String sex;

    public Persona (int age, String name, String sex) {
        this.age = age;
        this.name = name;
        this.sex = sex;
    }

    public String saluta() {
        return "Hi! My name is " + this.name + " and I'm a " + this.sex + " of " + this.age;
    }

    public int getAge() {
        return this.age;
    }
    
    public void mangia() {
        System.out.println("Gnan Gnam");
    }

    public void respira() {
        System.out.println("Respira");
    }

    public void muore() {
        System.out.println("AAAAAAAH!!");
    }

}