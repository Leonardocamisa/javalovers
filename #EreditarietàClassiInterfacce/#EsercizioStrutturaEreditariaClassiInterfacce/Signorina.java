public class Signorina extends Persona {

    public Signorina(int age, String name, String sex) {
        super(age, name, sex);
    }

    // @Override                     //Sovrascrive il metodo saluta definito nella classe astratta "Persona";
    // public String saluta() {
    //     return "Hi! My name is" + this.name + " and I'm a " + this.sex + " of... I won't tell you ";
    // }

}