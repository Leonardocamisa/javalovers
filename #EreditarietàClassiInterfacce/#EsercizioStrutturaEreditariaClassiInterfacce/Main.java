// Disegnare e codificare una struttura di classi ereditarie tra loro che implementano un'interfaccia.

// Deve esistere almeno:

// Una classe astratta
// Un'Interfaccia
// Una classe figlia della classe astratta
// Il Main che utilizza le classi nominate sopra

// Consiglio di disegnare a mano una struttura logica e poi di implementarla via codice
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Persona genericHuman;

        System.out.print("Inserisci maschio o femmina: ");

        String sex = sc.nextLine();
        if( sex.equals("maschio") ){
            genericHuman = new Signore(30, "Pippo", sex);
        } else {
            genericHuman = new Signorina(20, "Giovannina", sex);
        }

        System.out.println(genericHuman.saluta());
        genericHuman.muore();

    }
}
        