
public abstract class Animal extends Object implements Vivente {

    protected int age;
    protected String specie;
    protected String verso;
    protected String name;
    private boolean male;

    public Animal (int age, String verso, String name) {
        this.age = age;
        this.verso = verso;
        this.name = name;
    }

    @Override
    public String toString() {
        // return super.toString();
        return this.name + " " + this.verso;
    }

    public boolean isMale() {
        return this.male;
    }

    public int getAge() {
        return this.age;
    }

    public void mangia() {
        System.out.println("Mangia");
    }

    public void respira() {
        System.out.println("Respira");
    }

    public void muori() {
        System.out.println("Muore");
    }

    public boolean canCopulate(Vivente x) {
        return this.getMale() != x.getMale();
    }
}