
public class Main {

    public static void main(String[] args) {

        Felino gatto = new Felino(12, "miao", 100);

        Uccelli mario = new Uccelli(12, "chop", "mario", "cardellino");

        Animal genericAnimal = new Animal(34, "grr", "Gigi");

        Vivente x = new Umano();
        Vivente y = new Felino();

        x.muori();
        y.muori();

        System.out.println(gatto);
        System.out.println(mario);
        System.out.println(genericAnimal);

        salvaSuFile(mario);

        // System.out.println(gatto.toString());
    }

    public static void salvaSuFile(SalvabileSuFile x) {
        x.getFileName();
        x.writeObject();
    }

}