
public class Felino extends Animal {

    private int aggressivita;

    public void attack() {
        System.out.print("Attacco");
    }

    public void attack(int level) {
        System.out.print("Attacco di livello " + level);
    }

    public Felino(int age, String verso, int aggr) {
        super(age, verso, "silvestro");
        this.aggressivita = aggr;
    }

    @Override
    public String toString() {
        return "il felino fa " + this.verso;
    }

    @Override
    public int getAge() {
        return super.getAge() * 7;
    }

}