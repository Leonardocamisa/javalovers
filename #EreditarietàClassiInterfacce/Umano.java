
public class Umano implements Vivente {

    private boolean male;
    private String name;

    public Umano() {
        this.male = true;
    }
    public boolean isMale() {
        return this.male;
    }

    public void mangia() {
        System.out.println("Mangia");
    }

    public void respira() {
        System.out.println("Respira");
    }

    public void muori() {
        System.out.println("Muore");
    }

    public boolean canCopulate(Vivente x) {
        return this.getMale() != x.getMale();
    }

    // public String getFileName() {
    //     return this.name + ".txt";
    // }

}