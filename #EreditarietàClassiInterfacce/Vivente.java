
public interface Vivente {


    public boolean isMale();

    public void mangia();
    public void respira();
    public void muori();
    public boolean canCopulate(Vivente x); 
}