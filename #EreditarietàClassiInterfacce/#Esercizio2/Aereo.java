public class Aereo extends Veicolo {

    public static final int velocitaMin = 65;
    public static final int velocitaMax = 120;

    public Aereo(String name) {
        super(name);
    }

    public abstract int getVelocitaMax() {
        return velocitaMax;
    }
    public abstract int getVelocitaMin() {
        return velocitaMin;
    }
}