public abstract class Veicolo implements TipoVeicolo {

    public String name;
     

    public Veicolo (String name) {
        this.name = name;
    }

    public double findSpeed(){
        double speed = 0;
        speed = ((Math.random() * (getVelocitaMax() - getVelocitaMin())) + getVelocitaMin());
        return speed;
    }
    
    // public void rifornimento() {
    //     System.out.println("-100 euro");
    // }

    // public void inquinamento() {
    //     System.out.println("stai inquinando troppo");
    // }

    public abstract int getVelocitaMax();
    public abstract int getVelocitaMin();

}