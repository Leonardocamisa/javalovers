public class Auto extends Veicolo {

    public static final int velocitaMin = 50;
    public static final int velocitaMax = 80;

    public Auto(String name, int velocitaMin, int velocitaMax) {
        super(name, velocitaMin, velocitaMax);
    }

    public abstract int getVelocitaMax() {
        return velocitaMax;
    }
    public abstract int getVelocitaMin() {
        return velocitaMin;
    }

}