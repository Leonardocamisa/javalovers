// Programmare un piccolo gioco che rappresenta una gara tra 10 veicoli, in cui l'utente è uno di questi veicoli.

// I tipi di veicolo sono: 
// Automobili
// Motociclette
// Velivoli
// Ognuna di queste classi di veicoli ha un certo range di velocità generato alla creazione del veicolo (il veicolo spawna con una velocità casuale) basato su un range preimpostato per ogni tipo di veicolo.
// Automobili:  50-80 km/h
// Motociclette: 60-90 km/h
// Velivoli: 65-120 km/h

// Generare quindi 9 veicoli di tipo casuale tra i 3 e iniziare la gara su una tratta di 12km, stabilendo poi la classifica e i tempi di arrivo

public class Main {

    public static void main(String[] args) {
        System.out.println("Prontiiii??? ");

        Veicolo Veicolo1 = new Moto("yamaha");
        Veicolo Veicolo2 = new Auto("Toyota");
        Veicolo Veicolo3 = new Aereo("Boeing");

        System.out.println(Veicolo1.name + " " + Veicolo1.findSpeed() + "Km/h");
        System.out.println(Veicolo2.name + " " + Veicolo1.findSpeed() + "Km/h");
        System.out.println(Veicolo3.name + " " + Veicolo1.findSpeed() + "Km/h");


    }
}
  

