public class Moto extends Veicolo {

    public static final int velocitaMin = 60;
    public static final int velocitaMax = 90;

    public Moto(String name, int velocitaMin, int velocitaMax) {
        super(name, velocitaMin, velocitaMax);
    }

    public abstract int getVelocitaMax() {
        return velocitaMax;
    }

    public abstract int getVelocitaMin() {
        return velocitaMin;
    }
}