import java.util.Scanner;

public class CalcolaSequenzaMonete {

    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";

    public static void main (String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int cinquantaCent, ventiCent, dieciCent, cinqueCent, dueCent, unoCent;
        
        int value;
        System.out.println(ANSI_CYAN + "\t - - - - CALCOLATORE DI RESTO - - - -\n\n" + ANSI_YELLOW);
        System.out.print(" Inserisci un valore da dividere in monete: ");
        value = keyboard.nextInt();

        cinquantaCent = value / 50;
        value = value % 50;
        ventiCent = value / 20;
        value = value % 20;
        dieciCent = value / 10;
        value = value % 10;
        cinqueCent = value / 5;
        value = value % 5;
        dueCent = value / 2;
        value = value % 2;
        unoCent = value / 1;

        System.out.println("\n\t Ti serviranno: " + ANSI_GREEN);
        System.out.println("Monete da 50 cent: " + cinquantaCent);
        System.out.println("Monete da 20 cent: " + ventiCent);
        System.out.println("Monete da 10 cent: " + dieciCent);
        System.out.println("Monete da 5 cent: " + cinqueCent);
        System.out.println("Monete da 2 cent: " + dueCent);
        System.out.println("Monete da 1 cent: " + unoCent);

    }
}