import java.util.Scanner;
public class ArrayEx {


    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int currentVoto;
        int somma = 0;
        int[] voti = new int[10];

        for (int i = 0; i < voti.length; i++) {
            System.out.println(" Inserisci il " + (i+1) + " voto: ");
            currentVoto = sc.nextInt();
            voti[i] = currentVoto;
            somma += currentVoto;
        }

        for (int x: voti) {
            System.out.print(x + " ");
        }
        System.out.println("/n Voti Ordinati:");

        ordinaArray(voti);
        
        for(int i = 0; i < voti.length; i++){
            System.out.print(voti[i] + ",");
        }
    }


    public static int[] ordinaArray(int[] array) {
        int[] arrayOrdinato = new int[array.length];
        for(int i = 1; i < array.length; i++){
            int keyValue = array[i];
            int j = i - 1;
            while(j >= 0 && array[j] > keyValue){
                array[j + 1] = array[j];
                j = j - 1;
            }
            array[j + 1] = keyValue;
        }

        return arrayOrdinato;
    }
}